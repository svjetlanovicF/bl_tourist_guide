import {createClient} from 'npm:@supabase/supabase-js@2'
import {JWT} from 'npm:google-auth-library@9.4.0'

interface Comment {
  id: bigint
  comment: string
  author: string
  destination_id: bigint
  created_at: string
}

interface WebhookPayload {
  type: 'INSERT'
  table: string
  record: Comment
  schema: 'Public'
  old_record: null | Comment
}

const supabase = createClient(
  Deno.env.get('SUPABASE_URL')!,
  Deno.env.get('SUPABASE_SERVICE_ROLE_KEY')!
)

Deno.serve(async (req) => {
  
  const payload: WebhookPayload = await req.json()

  console.log(payload);

  const { data } = await supabase
  .from('profiles')
  .select('fcm_token')

  const destination = await supabase.from('destination')
  .select('title')
  .eq('id', payload.record.destination_id)
  .single()

  if (data == null) {
    throw new Error(`Failed to send notification to user`);
  }

  console.log(data)

  const responses = []

  for (const user of data) {
    const fcmToken = user.fcm_token as string

    const {default: serviceAccount} = await import('../service_account.json', {
      with: { type: 'json' },
    })
  
    const accessToken = await getAccessToken({clientEmail: serviceAccount.client_email, privateKey: serviceAccount.private_key})
  
    const res = await fetch(
      `https://fcm.googleapis.com/v1/projects/${serviceAccount.project_id}/messages:send`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`
        },
        body: JSON.stringify({
          message: {
            token: fcmToken,
            notification: {
              title: `New comment for ${destination.data?.title}`,
              body: `${payload.record.comment}`
            }
          }
        })
      }
    )
  
    const resData = await res.json()
    if (res.status < 200 || 299 < res.status) {
      throw resData
      
    }

    responses.push(resData)
  } 

  return new Response(
    JSON.stringify(responses),
    { headers: { "Content-Type": "application/json" } }
  );

})

const getAccessToken = ({
  clientEmail,
  privateKey
} : {
  clientEmail: string
  privateKey: string
}): Promise<string> => {
  return new Promise((resolve, reject) => {
    const jwtClient = new JWT({
      email: clientEmail,
      key: privateKey,
      scopes: ['https://www.googleapis.com/auth/firebase.messaging'],
    })
    jwtClient.authorize((err, tokens) => {
      if (err) {
        reject(err)
        return;
      }
      resolve(tokens!.access_token!)
    })
  })
}

