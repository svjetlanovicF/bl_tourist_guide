import 'package:get_it/get_it.dart';

import 'app_state.dart';
import 'features/authentication_bloc/domain/bloc/authentication_bloc.dart';
import 'features/comment_bloc/domain/bloc/comment_bloc.dart';
import 'features/dashboard_bloc/domain/bloc/bloc/dashboard_bloc.dart';
import 'features/favorite_bloc/data/providers/favorite_provider.dart';
import 'features/favorite_bloc/domain/bloc/favorite_bloc.dart';
import 'features/search_bloc/domain/bloc/search_bloc.dart';
import 'models/locale_model.dart';
import 'services/supabase_service.dart';

final getIt = GetIt.instance;

void setupGetIt() {
  getIt
    ..registerLazySingleton(AppState.new)
    ..registerLazySingleton(AuthenticationBloc.new)
    ..registerLazySingleton(SupabaseService.new)
    ..registerLazySingleton(DashboardBloc.new)
    ..registerLazySingleton(SearchBloc.new)
    ..registerLazySingleton(CommentBloc.new)
    ..registerLazySingleton(FavoriteProvider.new)
    ..registerLazySingleton(LocaleModel.new)
    ..registerLazySingleton(FavoriteBloc.new);
}
