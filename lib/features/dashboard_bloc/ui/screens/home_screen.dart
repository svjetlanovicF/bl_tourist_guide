import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../screens/custom_page_route.dart';
import '../../../../screens/hotel_screen.dart';
import '../../../../screens/restaurants_screen.dart';
import '../../../../widgets/items_divider.dart';
import '../../data/models/dashboard_response.dart';
import '../widgets/hotels_list.dart';
import '../widgets/restaurants_items_list.dart';
import '../widgets/screen_header.dart';
import '../widgets/tourist_places_horizontal_list.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({required this.dashboardResponse, super.key});

  final DashboardResponse dashboardResponse;

  @override
  Widget build(BuildContext context) {
    final localization = AppLocalizations.of(context)!;

    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ScreenHeader(
              title: localization.appName,
              description: localization.homeScreenMessage,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, bottom: 8),
              child: Text(
                  AppLocalizations.of(context)!.touristPlaces.toUpperCase(),
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.bold)),
            ),
            SizedBox(
              height: 200,
              child: TouristPlacesHorizontalList(
                touristPlaces: dashboardResponse.touristPlaces,
              ),
            ),
            const ItemsDivider(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(AppLocalizations.of(context)!.hotels.toUpperCase(),
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold)),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).push(CustomPageRoute(
                          child:
                              HotelScreen(hotels: dashboardResponse.hotels)));
                    },
                    child: Text(localization.viewAll),
                  ),
                ],
              ),
            ),
            HotelsList(hotels: dashboardResponse.hotels.take(3).toList()),
            const ItemsDivider(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(AppLocalizations.of(context)!.restaurants.toUpperCase(),
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold)),
                  TextButton(
                    onPressed: () => Navigator.of(context).push(CustomPageRoute(
                        child: RestaurantsScreen(
                            restaurants: dashboardResponse.restaurants))),
                    child: Text(localization.viewAll),
                  ),
                ],
              ),
            ),
            RestaurantsItemsList(
              restaurants: dashboardResponse.restaurants.take(3).toList(),
            ),
          ],
        ),
      ),
    );
  }
}
