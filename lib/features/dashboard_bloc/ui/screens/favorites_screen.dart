import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../service_locator.dart';
import '../../../../widgets/custom_loader.dart';
import '../../../favorite_bloc/domain/bloc/favorite_bloc.dart';
import '../../../favorite_bloc/ui/widgets/favorites_list.dart';
import '../widgets/screen_header.dart';

class FavoritesScreen extends StatefulWidget {
  const FavoritesScreen({super.key});

  @override
  State<FavoritesScreen> createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  FavoriteBloc get _favoriteBloc => getIt<FavoriteBloc>();

  @override
  Widget build(BuildContext context) {
    final localization = AppLocalizations.of(context)!;

    return BlocConsumer<FavoriteBloc, FavoriteState>(
      listener: (context, state) {
        if (state is FavoriteDeleted) {
          setState(() {});
        }
      },
      bloc: _favoriteBloc,
      builder: (context, state) {
        if (state is FavoriteFetchLoadSucces) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ScreenHeader(
                    title: localization.favorites,
                    description: localization.favoritesScreenMessage,
                  ),
                  if (state.favorites.isNotEmpty)
                    FavoritesList(items: state.favorites)
                  else
                    const Center(
                      child: Text(
                        'No saved items!',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                    ),
                ],
              ),
            ),
          );
        }
        return const CustomLoader();
      },
    );
  }
}
