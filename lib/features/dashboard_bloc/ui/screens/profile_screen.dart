import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../screens/custom_page_route.dart';
import '../../../../screens/login_screen.dart';
import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../../../authentication_bloc/data/user.dart';
import '../widgets/language_section.dart';
import '../widgets/profile_section.dart';
import '../widgets/screen_header.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({required this.user, super.key});

  final User user;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ScreenHeader(
            title: AppLocalizations.of(context)!.profile,
            description: AppLocalizations.of(context)!.profileScreenMessage,
          ),
          const SizedBox(height: 20),
          Container(
            margin: const EdgeInsets.all(12),
            padding: const EdgeInsetsDirectional.symmetric(vertical: 16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              children: [
                ProfileSection(
                    title: AppLocalizations.of(context)!.fullname,
                    value: user.fullname),
                const Divider(),
                ProfileSection(title: 'Email', value: user.email),
                const Divider(),
                const SizedBox(height: 20),
                TextButton(
                  onPressed: () {
                    getIt<SupabaseService>().signOut();
                    Navigator.of(context).pushAndRemoveUntil(
                        CustomPageRoute(child: const LoginScreen()),
                        (_) => false);
                  },
                  child: Text(
                    AppLocalizations.of(context)!.signOut,
                    style: const TextStyle(
                      color: Colors.red,
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const LanguageSection(),
        ],
      ),
    ));
  }
}
