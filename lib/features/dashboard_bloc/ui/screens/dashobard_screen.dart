import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../service_locator.dart';
import '../../../favorite_bloc/domain/bloc/favorite_bloc.dart';
import '../../../search_bloc/ui/screens/search_screen.dart';
import '../../data/models/dashboard_response.dart';
import 'favorites_screen.dart';
import 'home_screen.dart';
import 'profile_screen.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({required this.dashboardResponse, super.key});

  final DashboardResponse dashboardResponse;

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  late AppLocalizations _localization;
  final _pageViewController = PageController();
  int _selectedIndex = 0;
  final _favoriteBloc = getIt<FavoriteBloc>();

  @override
  void initState() {
    super.initState();
    _favoriteBloc.add(FavoriteFetch());
  }

  @override
  void didChangeDependencies() {
    _localization = AppLocalizations.of(context)!;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      body: PageView(
        controller: _pageViewController,
        onPageChanged: _onPageChanged,
        children: [
          HomeScreen(dashboardResponse: widget.dashboardResponse),
          const FavoritesScreen(),
          const SearchScreen(),
          ProfileScreen(
            user: widget.dashboardResponse.user,
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        currentIndex: _selectedIndex,
        onTap: (index) => _pageViewController.animateToPage(index,
            duration: const Duration(milliseconds: 2), curve: Curves.bounceOut),
        type: BottomNavigationBarType.fixed,
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.blue,
        items: [
          BottomNavigationBarItem(
              icon: const Icon(Icons.home), label: _localization.home),
          BottomNavigationBarItem(
              icon: const Icon(Icons.favorite), label: _localization.favorites),
          BottomNavigationBarItem(
              icon: const Icon(Icons.search), label: _localization.search),
          BottomNavigationBarItem(
              icon: const Icon(Icons.person), label: _localization.profile),
        ],
      ),
    );
  }

  void _onPageChanged(int index) => setState(() {
        _selectedIndex = index;
      });
}
