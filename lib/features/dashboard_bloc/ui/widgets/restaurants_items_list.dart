import 'package:flutter/material.dart';

import '../../../../screens/custom_page_route.dart';
import '../../../../screens/restaurant_details_screen.dart';
import '../../data/models/restaurant.dart';

class RestaurantsItemsList extends StatelessWidget {
  const RestaurantsItemsList({required this.restaurants, super.key});

  final List<Restaurant> restaurants;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: restaurants
            .map(
              (restaurant) => GestureDetector(
                onTap: () {
                  Navigator.of(context).push(CustomPageRoute(
                      child: RestaurantDetailsScreen(restaurant: restaurant)));
                },
                child: ListTile(
                  leading: const Icon(Icons.restaurant),
                  title: Text(restaurant.title),
                  trailing: const Icon(Icons.chevron_right),
                ),
              ),
            )
            .toList());
  }
}
