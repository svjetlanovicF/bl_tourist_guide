import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../models/locale_model.dart';
import '../../../../service_locator.dart';
import '../../../../services/shared_pref_service.dart';

class LanguageSection extends StatefulWidget {
  const LanguageSection({super.key});

  @override
  State<LanguageSection> createState() => _LanguageSectionState();
}

class _LanguageSectionState extends State<LanguageSection> {
  String? selectedOption;
  late String _selectedLocale;

  final locale = getIt<LocaleModel>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _selectedLocale = Localizations.localeOf(context).toString();

    if (_selectedLocale == 'sr') {
      selectedOption = 'sr';
    } else {
      selectedOption = 'en';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(12),
      padding: const EdgeInsetsDirectional.symmetric(vertical: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        children: [
          Text(AppLocalizations.of(context)!.chooseLanguage,
              style: const TextStyle(fontSize: 16)),
          RadioListTile(
            title: Text(AppLocalizations.of(context)!.englishLanguage),
            activeColor: Colors.blue,
            value: 'en',
            groupValue: selectedOption,
            onChanged: (value) async => changeLanguage(value),
          ),
          RadioListTile(
            title: Text(AppLocalizations.of(context)!.serbianLanguage),
            activeColor: Colors.blue,
            value: 'sr',
            groupValue: selectedOption,
            onChanged: (value) async => changeLanguage(value),
          ),
        ],
      ),
    );
  }

  Future<void> changeLanguage(String? value) async {
    setState(() {
      selectedOption = value!;
      _selectedLocale = value;
      locale.set(Locale(_selectedLocale));
    });

    final sharedPrefService = await SharedPrefService.instance;
    await sharedPrefService?.saveLocale(_selectedLocale);
  }
}
