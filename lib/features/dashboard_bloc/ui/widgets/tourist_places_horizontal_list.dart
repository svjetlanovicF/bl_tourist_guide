import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';

import '../../../../screens/custom_page_route.dart';
import '../../../../screens/tourist_place_screen.dart';
import '../../data/models/tourist_place.dart';

class TouristPlacesHorizontalList extends StatelessWidget {
  const TouristPlacesHorizontalList({required this.touristPlaces, super.key});

  final List<TouristPlace> touristPlaces;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: touristPlaces.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () => Navigator.of(context).push(CustomPageRoute(
                child: TouristPlaceScreen(touristPlace: touristPlaces[index]))),
            child: Container(
              width: 200,
              margin: EdgeInsets.only(
                  left: 16, right: index == touristPlaces.length - 1 ? 16 : 0),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(6),
                  bottom: Radius.circular(6),
                ),
                color: Colors.blue,
              ),
              child: Stack(
                alignment: Alignment.bottomLeft,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: double.infinity,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.vertical(
                        top: Radius.circular(6),
                        bottom: Radius.circular(6),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: touristPlaces[index].image ?? '',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  ClipRRect(
                    clipBehavior: Clip.hardEdge,
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 2,
                        sigmaY: 2,
                        tileMode: TileMode.mirror,
                      ),
                      child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.6),
                        ),
                        child: Text(
                          touristPlaces[index].title,
                          maxLines: 2,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
