import 'package:flutter/material.dart';

class ScreenHeader extends StatelessWidget {
  const ScreenHeader(
      {required this.title, required this.description, super.key});

  final String title;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(fontSize: 20)),
          Text(description, style: const TextStyle(fontSize: 15)),
          const SizedBox(height: 16),
        ],
      ),
    );
  }
}
