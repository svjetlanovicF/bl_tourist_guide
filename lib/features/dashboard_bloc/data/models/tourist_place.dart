import '../../../../enums.dart';
import 'destination.dart';

class TouristPlace extends Destination {
  TouristPlace({
    required super.id,
    required super.title,
    required super.description,
    super.filter = Filter.touristPlace,
    super.location,
    super.image,
  });

  factory TouristPlace.fromJson({required Map<String, dynamic> json}) =>
      TouristPlace(
          id: json['id'],
          title: json['title'],
          description: json['description'],
          location: json['location'],
          image: json['image']);
}
