import '../../../../enums.dart';
import 'destination.dart';

class Hotel extends Destination {
  Hotel({
    required super.id,
    required super.title,
    required super.description,
    required this.stars,
    super.filter = Filter.hotel,
    super.image,
    super.location,
  });

  factory Hotel.fromJson({required Map<String, dynamic> json}) {
    final destinationJson = json['destination'] as Map<String, dynamic>;
    return Hotel(
      id: json['id'],
      title: destinationJson['title'],
      description: destinationJson['description'],
      image: destinationJson['image'],
      location: destinationJson['location'],
      stars: double.parse(json['stars'].toString()),
    );
  }

  final double stars;
}
