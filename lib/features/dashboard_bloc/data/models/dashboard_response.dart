import '../../../authentication_bloc/data/user.dart';
import 'hotel.dart';
import 'restaurant.dart';
import 'tourist_place.dart';

class DashboardResponse {
  const DashboardResponse({
    required this.touristPlaces,
    required this.restaurants,
    required this.hotels,
    required this.user,
  });

  final List<TouristPlace> touristPlaces;
  final List<Restaurant> restaurants;
  final List<Hotel> hotels;
  final User user;
}
