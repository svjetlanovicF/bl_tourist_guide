import '../../../../enums.dart';
import 'destination.dart';

class Restaurant extends Destination {
  Restaurant({
    required super.id,
    required super.title,
    required super.description,
    super.filter = Filter.restaurant,
    super.location,
    super.image,
    this.webUrl,
    this.priceRange,
  });

  factory Restaurant.fromJson({required Map<String, dynamic> json}) {
    final destinationJson = json['destination'] as Map<String, dynamic>;
    return Restaurant(
        id: destinationJson['id'],
        title: destinationJson['title'],
        description: destinationJson['description'],
        location: destinationJson['location'],
        image: destinationJson['image'],
        webUrl: json['web_url'],
        priceRange: json['price_range']);
  }

  String? webUrl;
  String? priceRange;
}
