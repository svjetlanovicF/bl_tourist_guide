import '../../../../enums.dart';

abstract class Destination {
  const Destination({
    required this.id,
    required this.title,
    required this.description,
    required this.filter,
    this.location,
    this.image,
  });

  final int id;
  final String title;
  final String description;
  final String? location;
  final String? image;
  final Filter filter;
}
