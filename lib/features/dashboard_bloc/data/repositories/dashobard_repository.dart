import '../../../../app_state.dart';
import '../../../../constants.dart';
import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../../../authentication_bloc/data/user.dart';
import '../models/dashboard_response.dart';
import '../models/hotel.dart';
import '../models/restaurant.dart';
import '../models/tourist_place.dart';

class DashboardRepository {
  final _supabaseService = getIt<SupabaseService>();
  final _appState = getIt<AppState>();

  Future<DashboardResponse> fetchAllDashboardData() async {
    final response = await Future.wait([
      _getTouristPlaces(),
      _getRestaurants(),
      _getHotels(),
      _getUserData(),
    ]);

    final touristPlaces = response[0] as List<TouristPlace>;
    final restaurants = response[1] as List<Restaurant>;
    final hotels = response[2] as List<Hotel>;
    final user = response[3] as User;

    _appState.setAppStateProperties(
      tr: touristPlaces,
      r: restaurants,
      user: user,
      h: hotels,
    );

    return DashboardResponse(
      touristPlaces: touristPlaces,
      restaurants: restaurants,
      user: user,
      hotels: hotels,
    );
  }

  Future<List<TouristPlace>> _getTouristPlaces() async {
    final rawTouristPlaces = await _supabaseService.supabaseClient
        .from(TableNames.touristPlaceTable)
        .select('*, ${TableNames.destinationTable}!inner(*)') as List;

    return rawTouristPlaces
        .map((json) => TouristPlace.fromJson(
            json: (json as Map<String, dynamic>)['destination']))
        .toList();
  }

  Future<List<Restaurant>> _getRestaurants() async {
    final rawRestaurants = await _supabaseService.supabaseClient
        .from(TableNames.restaurantTable)
        .select('*, ${TableNames.destinationTable}!inner(*)') as List<dynamic>;

    return rawRestaurants
        .map((json) => Restaurant.fromJson(json: json))
        .toList();
  }

  Future<List<Hotel>> _getHotels() async {
    final rawHotels = await _supabaseService.supabaseClient
        .from(TableNames.hotelTable)
        .select('*, ${TableNames.destinationTable}!inner(*)') as List<dynamic>;

    return rawHotels.map((json) => Hotel.fromJson(json: json)).toList();
  }

  Future<User> _getUserData() async {
    final userId = _supabaseService.currentUser?.id;
    final rawUserData = await _supabaseService.supabaseClient
        .from(TableNames.userTable)
        .select()
        .eq('id', userId)
        .single() as Map<String, dynamic>;

    return User.fromJson(json: rawUserData);
  }
}
