import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../data/models/dashboard_response.dart';
import '../../../data/repositories/dashobard_repository.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  DashboardBloc() : super(DashboardInitial()) {
    on<DashboardFetchDashboardModels>(_getDashboardData);
  }

  final _dashboardRepository = DashboardRepository();

  Future<void> _getDashboardData(
      DashboardFetchDashboardModels event, Emitter<DashboardState> emit) async {
    emit(DashboardInitial());

    final dashboardResponse =
        await _dashboardRepository.fetchAllDashboardData();

    emit(DashboardLoadSucces(dashboardResponse: dashboardResponse));
  }
}
