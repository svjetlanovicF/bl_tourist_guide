part of 'dashboard_bloc.dart';

sealed class DashboardState extends Equatable {
  const DashboardState();

  @override
  List<Object> get props => [];
}

final class DashboardInitial extends DashboardState {}

final class DashboardLoadSucces extends DashboardState {
  const DashboardLoadSucces({required this.dashboardResponse});

  final DashboardResponse dashboardResponse;

  @override
  List<Object> get props => [dashboardResponse];
}

final class DashboardSearchLoadSucces extends DashboardState {
  const DashboardSearchLoadSucces({required this.dashboardResponse});

  final DashboardResponse dashboardResponse;

  @override
  List<Object> get props => [dashboardResponse];
}
