import '../../../../constants.dart';
import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../models/comment.dart';

class CommentRepository {
  final _supabaseService = getIt<SupabaseService>();

  Future<List<Comment>> fetchComments(int destinationId) async {
    final rawComments = await _supabaseService.supabaseClient
        .from(TableNames.commentTable)
        .select(
            '*, ${TableNames.destinationTable}!inner(*), ${TableNames.userTable}!inner(fullname)')
        .filter('destination_id', 'eq', '$destinationId')
        .order('created_at') as List;

    final comments = rawComments
        .map((e) => Comment.fromJson(e as Map<String, dynamic>))
        .toList();

    return comments;
  }

  Future<void> addComment(Comment comment) async {
    if (comment.comment.isNotEmpty) {
      await _supabaseService.supabaseClient
          .from(TableNames.commentTable)
          .insert(comment.toJson());
    }
  }

  Future<void> removeComment(int commentId) async {
    await _supabaseService.supabaseClient
        .from(TableNames.commentTable)
        .delete()
        .eq('id', commentId);
  }
}
