class Comment {
  const Comment({
    required this.comment,
    required this.authorId,
    required this.createdAt,
    required this.destinationId,
    this.authorName,
    this.id,
  });

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json['id'],
        comment: json['comment'],
        authorId: json['author'],
        createdAt: DateTime.parse(json['created_at']),
        destinationId: json['destination_id'],
        authorName: (json['user'] as Map<String, dynamic>)['fullname'],
      );

  Map<String, dynamic> toJson() => {
        'comment': comment,
        'author': authorId,
        'created_at': createdAt.toString(),
        'destination_id': destinationId,
      };

  final int? id;
  final String comment;
  final String authorId;
  final int destinationId;
  final DateTime createdAt;
  final String? authorName;
}
