import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../data/models/comment.dart';
import 'comments_list.dart';

class CommentSection extends StatelessWidget {
  const CommentSection({required this.comments, super.key});

  final List<Comment> comments;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(context)!.comments,
          style: const TextStyle(fontSize: 20),
        ),
        CommentsList(
          comments: comments,
        ),
        const SizedBox(height: 100)
      ],
    );
  }
}
