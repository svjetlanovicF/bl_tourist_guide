import 'package:flutter/material.dart';

import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../../data/models/comment.dart';
import '../../domain/bloc/comment_bloc.dart';

class CommentsList extends StatefulWidget {
  const CommentsList({required this.comments, super.key});

  final List<Comment> comments;

  @override
  State<CommentsList> createState() => _CommentsListState();
}

class _CommentsListState extends State<CommentsList> {
  final String userId = getIt<SupabaseService>().currentUser!.id;

  final _commentBloc = getIt<CommentBloc>();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: widget.comments.length,
      itemBuilder: (context, index) {
        return Container(
          padding: const EdgeInsets.all(6),
          margin: const EdgeInsets.only(bottom: 4),
          decoration: BoxDecoration(
            color: Colors.blue.shade100,
            borderRadius: BorderRadius.circular(6),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.comments[index].authorName ?? '',
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    widget.comments[index].comment,
                    style: const TextStyle(color: Colors.black, fontSize: 14),
                  ),
                ],
              ),
              if (widget.comments[index].authorId == userId)
                IconButton(
                  onPressed: () {
                    setState(() {
                      _commentBloc.add(
                          RemoveComment(commentId: widget.comments[index].id!));
                    });
                  },
                  icon: const Icon(Icons.delete),
                ),
            ],
          ),
        );
      },
    );
  }
}
