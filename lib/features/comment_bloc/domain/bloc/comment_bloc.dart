import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../data/models/comment.dart';
import '../../data/repositories/comment_repository.dart';

part 'comment_event.dart';
part 'comment_state.dart';

class CommentBloc extends Bloc<CommentEvent, CommentState> {
  CommentBloc() : super(CommentInitial()) {
    on<FetchComments>(_fetchComments);
    on<AddComment>(_addComment);
    on<RemoveComment>(_removeComment);
  }

  final _commentRepo = CommentRepository();
  List<Comment> _comments = [];

  Future<void> _fetchComments(
      FetchComments event, Emitter<CommentState> emit) async {
    emit(CommentInitial());
    final comments = await _commentRepo.fetchComments(event.destinationId);
    _comments = comments;

    emit(CommentsFetchLoadSucces(comments: comments));
  }

  void _addComment(AddComment event, Emitter<CommentState> emit) {
    unawaited(_commentRepo.addComment(event.comment));
    _comments.insert(0, event.comment);
    emit(CommentsFetchLoadSucces(comments: _comments));
  }

  void _removeComment(RemoveComment event, Emitter<CommentState> emit) {
    unawaited(_commentRepo.removeComment(event.commentId));
    _comments.removeWhere((element) => element.id == event.commentId);
    emit(CommentsFetchLoadSucces(comments: _comments));
  }
}
