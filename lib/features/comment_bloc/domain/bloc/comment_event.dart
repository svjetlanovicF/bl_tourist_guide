part of 'comment_bloc.dart';

sealed class CommentEvent extends Equatable {
  const CommentEvent();

  @override
  List<Object> get props => [];
}

class FetchComments extends CommentEvent {
  const FetchComments({required this.destinationId});

  final int destinationId;

  @override
  List<Object> get props => [destinationId];
}

class AddComment extends CommentEvent {
  const AddComment({required this.comment});

  final Comment comment;

  @override
  List<Object> get props => [comment];
}

class RemoveComment extends CommentEvent {
  const RemoveComment({required this.commentId});

  final int commentId;

  @override
  List<Object> get props => [commentId];
}
