part of 'comment_bloc.dart';

sealed class CommentState extends Equatable {
  const CommentState();

  @override
  List<Object> get props => [];
}

final class CommentInitial extends CommentState {}

class CommentsFetchLoadSucces extends CommentState {
  const CommentsFetchLoadSucces({required this.comments});

  final List<Comment> comments;

  // @override
  // List<Object> get props => [comments];
}
