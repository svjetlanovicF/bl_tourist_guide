part of 'favorite_bloc.dart';

sealed class FavoriteEvent extends Equatable {
  const FavoriteEvent();

  @override
  List<Object> get props => [];
}

class FavoriteFetch extends FavoriteEvent {}

class FavoriteAdd extends FavoriteEvent {
  const FavoriteAdd({required this.favorite});

  final Favorite favorite;

  @override
  List<Object> get props => [favorite];
}

class FavoriteDelete extends FavoriteEvent {
  const FavoriteDelete({required this.destinationId});

  final int destinationId;

  @override
  List<Object> get props => [destinationId];
}
