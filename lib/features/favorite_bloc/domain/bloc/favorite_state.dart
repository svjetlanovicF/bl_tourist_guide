part of 'favorite_bloc.dart';

sealed class FavoriteState extends Equatable {
  const FavoriteState();

  @override
  List<Object> get props => [];
}

final class FavoriteInitial extends FavoriteState {}

class FavoriteFetchLoadSucces extends FavoriteState {
  const FavoriteFetchLoadSucces({required this.favorites});

  final List<Favorite> favorites;

  @override
  List<Object> get props => [favorites];
}

class FavoriteDeleted extends FavoriteState {}
