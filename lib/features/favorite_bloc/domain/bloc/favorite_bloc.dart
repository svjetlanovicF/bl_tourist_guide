import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../service_locator.dart';
import '../../data/models/favorite.dart';
import '../../data/providers/favorite_provider.dart';

part 'favorite_event.dart';
part 'favorite_state.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  FavoriteBloc() : super(FavoriteInitial()) {
    on<FavoriteFetch>(_getFavorites);
    on<FavoriteAdd>(_addFavorite);
    on<FavoriteDelete>(_deleteFavorite);
  }

  final _favoriteProvider = getIt<FavoriteProvider>();

  List<Favorite> _favorites = [];

  Future<void> _getFavorites(
    FavoriteFetch event,
    Emitter<FavoriteState> emit,
  ) async {
    if (_favorites.isNotEmpty) {
      emit(FavoriteFetchLoadSucces(favorites: _favorites));
    }
    emit(FavoriteInitial());
    _favorites = await _favoriteProvider.getFavorites();

    emit(FavoriteFetchLoadSucces(favorites: _favorites));
  }

  void _addFavorite(FavoriteAdd event, Emitter<FavoriteState> emit) {
    unawaited(_favoriteProvider.insert(event.favorite));

    _favorites.insert(0, event.favorite);
    emit(FavoriteFetchLoadSucces(favorites: _favorites));
  }

  void _deleteFavorite(FavoriteDelete event, Emitter<FavoriteState> emit) {
    unawaited(_favoriteProvider.delete(event.destinationId));

    _favorites.removeWhere((e) => e.destinationId == event.destinationId);
    emit(FavoriteDeleted());
    emit(FavoriteFetchLoadSucces(favorites: _favorites));
  }

  bool isFavorite(int id) => _favorites.any((e) => e.destinationId == id);
}
