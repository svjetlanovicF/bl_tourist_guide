import 'package:flutter/material.dart';

import '../../../../enums.dart';
import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../../data/models/favorite.dart';
import '../../domain/bloc/favorite_bloc.dart';

class FavoriteIcon extends StatefulWidget {
  const FavoriteIcon(
      {required this.id,
      required this.title,
      required this.type,
      required this.isFromFavoriteList,
      super.key});

  final int id;
  final String title;
  final Filter type;
  final bool isFromFavoriteList;

  @override
  State<FavoriteIcon> createState() => _FavoriteIconState();
}

class _FavoriteIconState extends State<FavoriteIcon> {
  bool _isSaved = false;
  final _favoriteBloc = getIt<FavoriteBloc>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _isSaved = _favoriteBloc.isFavorite(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: _saveItem,
      icon: Icon(
        _isSaved ? Icons.favorite : Icons.favorite_border_outlined,
        color: _isSaved ? Colors.red : Colors.white,
        size: 30,
      ),
    );
  }

  void _saveItem() {
    setState(() {
      _isSaved = !_isSaved;
    });

    if (_isSaved) {
      _favoriteBloc.add(
        FavoriteAdd(
          favorite: Favorite(
              destinationId: widget.id,
              title: widget.title,
              filter: widget.type.translate(context),
              userId: getIt<SupabaseService>().currentUser!.id),
        ),
      );
    } else {
      _favoriteBloc.add(FavoriteDelete(destinationId: widget.id));

      if (widget.isFromFavoriteList) {
        Navigator.of(context).pop();
      }
    }
  }
}
