import 'package:flutter/material.dart';

import '../../../../app_state.dart';
import '../../../../enums.dart';
import '../../../../screens/custom_page_route.dart';
import '../../../../screens/hotel_details_screen.dart';
import '../../../../screens/restaurant_details_screen.dart';
import '../../../../screens/tourist_place_screen.dart';
import '../../../../service_locator.dart';
import '../../data/models/favorite.dart';

class FavoritesList extends StatelessWidget {
  const FavoritesList({required this.items, super.key});

  final List<Favorite> items;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: items.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () => _navigateToScreen(index, context),
          child: Container(
            margin: const EdgeInsets.only(bottom: 8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            child: ListTile(
              leading: Icon(_getIcon(index)),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    items[index].filter,
                    style: const TextStyle(fontSize: 12, color: Colors.grey),
                  ),
                  Text(items[index].title)
                ],
              ),
              trailing: const Icon(Icons.chevron_right),
            ),
          ),
        );
      },
    );
  }

  void _navigateToScreen(int index, BuildContext context) {
    final appState = getIt<AppState>();

    if (Filter.fromString(items[index].filter) == Filter.restaurant) {
      final restaurant = appState.restaurants
          .where((e) => e.id == items[index].destinationId)
          .first;
      Navigator.of(context).push(CustomPageRoute(
          child: RestaurantDetailsScreen(
        restaurant: restaurant,
        isFromFavoriteList: true,
      )));
    } else if (Filter.fromString(items[index].filter) == Filter.hotel) {
      final hotel = appState.hotels
          .where((e) => e.id == items[index].destinationId)
          .first;
      Navigator.of(context).push(CustomPageRoute(
          child: HotelDetailsScreen(
        hotel: hotel,
        isFromFavoriteList: true,
      )));
    } else {
      final touristPlace = appState.touristPlaces
          .where((e) => e.id == items[index].destinationId)
          .first;
      Navigator.of(context).push(CustomPageRoute(
          child: TouristPlaceScreen(
        touristPlace: touristPlace,
        isFromFavoriteList: true,
      )));
    }
  }

  IconData _getIcon(int index) {
    if (Filter.fromString(items[index].filter) == Filter.restaurant) {
      return Icons.restaurant;
    } else if (Filter.fromString(items[index].filter) == Filter.hotel) {
      return Icons.hotel;
    }

    return Icons.travel_explore;
  }
}
