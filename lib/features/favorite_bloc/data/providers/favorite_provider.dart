import 'package:sqflite/sqflite.dart';

import '../../../../constants.dart';
import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../models/favorite.dart';

class FavoriteProvider {
  late Database _db;

  Future<void> openConnection() async {
    _db = await openDatabase('favorite_db.db', version: 2,
        onCreate: (db, version) async {
      await db.execute('''
create table $favoriteTable (
  $columnId integer primary key autoincrement,
  $columnDestinationId integer not null,
  $columnTitle text not null,
  $columnFilter text not null,
  $columnUserId text not null)
''');
    });
  }

  Future<void> insert(Favorite favorite) async {
    await _db.insert(favoriteTable, favorite.toMap());
  }

  Future<List<Favorite>> getFavorites() async {
    final userId = getIt<SupabaseService>().currentUser?.id;
    final List<Map<String, dynamic>> rawFavorites = await _db
        .query(favoriteTable, where: '$columnUserId = ?', whereArgs: [userId]);

    return rawFavorites.map(Favorite.fromMap).toList();
  }

  Future<int> delete(int id) async => _db.delete(favoriteTable,
      where: 'destinationId = ? AND userId = ?',
      whereArgs: [id, getIt<SupabaseService>().currentUser!.id]);

  Future<void> deleteTable() async {
    final dbpath = await getDatabasesPath();
    final path = '$dbpath/favorite.db';

    await deleteDatabase(path);
  }

  Future close() async => _db.close();
}
