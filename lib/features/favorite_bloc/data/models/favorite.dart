import '../../../../constants.dart';

class Favorite {
  const Favorite({
    required this.destinationId,
    required this.title,
    required this.filter,
    required this.userId,
    this.id,
  });

  factory Favorite.fromMap(Map<String, dynamic> map) => Favorite(
        id: map[columnId],
        title: map[columnTitle],
        filter: map[columnFilter],
        userId: map[columnUserId],
        destinationId: map[columnDestinationId],
      );

  Map<String, dynamic> toMap() => {
        columnDestinationId: destinationId,
        columnTitle: title,
        columnFilter: filter,
        columnUserId: userId,
      };

  final int? id;
  final int destinationId;
  final String title;
  final String filter;
  final String userId;
}
