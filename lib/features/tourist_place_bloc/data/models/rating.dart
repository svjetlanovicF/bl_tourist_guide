class Rating {
  const Rating({
    required this.rate,
    required this.touristPlacesId,
    required this.userId,
    this.id,
  });

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
      id: json['id'],
      rate: (json['rate'] as int).toDouble(),
      touristPlacesId: json['tourist_place_id'],
      userId: json['user_id']);

  Map<String, dynamic> toJson() => {
        'rate': rate,
        'tourist_place_id': touristPlacesId,
        'user_id': userId,
      };

  final int? id;
  final double rate;
  final int touristPlacesId;
  final String userId;
}
