import '../../../../app_state.dart';
import '../../../../enums.dart';
import '../../../../service_locator.dart';
import '../../../dashboard_bloc/data/models/destination.dart';

class SearchRepository {
  final _appState = getIt<AppState>();

  Future<List<Destination>> search(String query, Filter filter) async {
    final destinations = <Destination>[];

    if (query.length < 2) {
      return destinations;
    }

    await Future.delayed(const Duration(milliseconds: 300)).then((value) {
      if (filter == Filter.touristPlace) {
        destinations.addAll(_appState.touristPlaces
            .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
            .toList());
      } else if (filter == Filter.hotel) {
        destinations.addAll(_appState.hotels
            .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
            .toList());
      } else if (filter == Filter.restaurant) {
        destinations.addAll(_appState.restaurants
            .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
            .toList());
      } else {
        destinations
          ..addAll(_appState.restaurants
              .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
              .toList())
          ..addAll(_appState.touristPlaces
              .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
              .toList())
          ..addAll(_appState.hotels
              .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
              .toList());
      }
    });

    return destinations;
  }
}
