part of 'search_bloc.dart';

sealed class SearchEvent extends Equatable {
  const SearchEvent();

  @override
  List<Object> get props => [];
}

class SearchItemsEvent extends SearchEvent {
  const SearchItemsEvent({required this.query, required this.filter});
  final String query;
  final Filter filter;

  @override
  List<Object> get props => [query, filter];
}
