part of 'search_bloc.dart';

sealed class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

final class SearchInitial extends SearchState {}

class SearchLoadingState extends SearchState {}

class SearchLoadSucces extends SearchState {
  const SearchLoadSucces({required this.response});
  final List<Destination> response;

  @override
  List<Object> get props => [response];
}
