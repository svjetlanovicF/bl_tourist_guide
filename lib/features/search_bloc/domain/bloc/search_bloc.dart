import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../enums.dart';
import '../../../dashboard_bloc/data/models/destination.dart';
import '../../data/repositories/search_repository.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc() : super(SearchInitial()) {
    on<SearchItemsEvent>(_search);
  }
  final _searchRepository = SearchRepository();

  Future<void> _search(
      SearchItemsEvent event, Emitter<SearchState> emit) async {
    emit(SearchLoadingState());

    final destinations =
        await _searchRepository.search(event.query, event.filter);
    emit(SearchLoadSucces(response: destinations));
  }
}
