import 'package:flutter/material.dart';

import '../../../../enums.dart';
import '../../../../screens/custom_page_route.dart';
import '../../../../screens/hotel_details_screen.dart';
import '../../../../screens/restaurant_details_screen.dart';
import '../../../../screens/tourist_place_screen.dart';
import '../../../dashboard_bloc/data/models/destination.dart';
import '../../../dashboard_bloc/data/models/hotel.dart';
import '../../../dashboard_bloc/data/models/restaurant.dart';
import '../../../dashboard_bloc/data/models/tourist_place.dart';

class SearchItems extends StatelessWidget {
  const SearchItems({required this.destinations, super.key});

  final List<Destination> destinations;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: destinations.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () => _navigateToScreen(index, context),
          behavior: HitTestBehavior.opaque,
          child: Column(
            children: [
              ListTile(
                leading: Icon(_getIconWithFilter(index)),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      destinations[index].filter.translate(context),
                      style: const TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    Text(destinations[index].title)
                  ],
                ),
                trailing: const Icon(Icons.chevron_right),
              ),
              const Divider(),
            ],
          ),
        );
      },
    );
  }

  void _navigateToScreen(int index, BuildContext context) {
    if (destinations[index].filter == Filter.restaurant) {
      Navigator.of(context).push(CustomPageRoute(
          child: RestaurantDetailsScreen(
              restaurant: destinations[index] as Restaurant)));
    } else if (destinations[index].filter == Filter.hotel) {
      Navigator.of(context).push(CustomPageRoute(
          child: HotelDetailsScreen(hotel: destinations[index] as Hotel)));
    } else {
      Navigator.of(context).push(CustomPageRoute(
          child: TouristPlaceScreen(
              touristPlace: destinations[index] as TouristPlace)));
    }
  }

  IconData _getIconWithFilter(int index) {
    if (destinations[index].filter == Filter.restaurant) {
      return Icons.restaurant;
    } else if (destinations[index].filter == Filter.hotel) {
      return Icons.hotel;
    }

    return Icons.travel_explore;
  }
}
