import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../enums.dart';
import '../../../../widgets/custom_loader.dart';
import '../../../dashboard_bloc/ui/widgets/screen_header.dart';
import '../../domain/bloc/search_bloc.dart';
import '../widgets/search_items_list.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  late AppLocalizations _localization;
  Filter _selectedFilter = Filter.all;
  final TextEditingController _searchController = TextEditingController();
  final _searchBloc = SearchBloc();

  @override
  void didChangeDependencies() {
    _localization = AppLocalizations.of(context)!;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ScreenHeader(
                title: _localization.search,
                description: _localization.searchScreenMessage),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CupertinoSearchTextField(
                    controller: _searchController,
                    onChanged: (value) {
                      _searchBloc.add(SearchItemsEvent(
                          query: value, filter: _selectedFilter));
                    },
                  ),
                  const SizedBox(height: 8),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Wrap(
                      spacing: 8,
                      children: [
                        ChoiceChip(
                          checkmarkColor: _selectedFilter == Filter.all
                              ? Colors.white
                              : Colors.black,
                          label: Text(
                            Filter.all.translate(context),
                            style: TextStyle(
                              color: _selectedFilter == Filter.all
                                  ? Colors.white
                                  : Colors.black,
                            ),
                          ),
                          selected: _selectedFilter == Filter.all,
                          selectedColor: Colors.blue,
                          onSelected: (value) => _changeFilter(Filter.all),
                        ),
                        ChoiceChip(
                          checkmarkColor: _selectedFilter == Filter.touristPlace
                              ? Colors.white
                              : Colors.black,
                          label: Text(
                            Filter.touristPlace.translate(context),
                            style: TextStyle(
                              color: _selectedFilter == Filter.touristPlace
                                  ? Colors.white
                                  : Colors.black,
                            ),
                          ),
                          selected: _selectedFilter == Filter.touristPlace,
                          selectedColor: Colors.blue,
                          onSelected: (value) =>
                              _changeFilter(Filter.touristPlace),
                        ),
                        ChoiceChip(
                          checkmarkColor: _selectedFilter == Filter.hotel
                              ? Colors.white
                              : Colors.black,
                          label: Text(
                            Filter.hotel.translate(context),
                            style: TextStyle(
                              color: _selectedFilter == Filter.hotel
                                  ? Colors.white
                                  : Colors.black,
                            ),
                          ),
                          selected: _selectedFilter == Filter.hotel,
                          selectedColor: Colors.blue,
                          onSelected: (value) => _changeFilter(Filter.hotel),
                        ),
                        ChoiceChip(
                          checkmarkColor: _selectedFilter == Filter.restaurant
                              ? Colors.white
                              : Colors.black,
                          label: Text(
                            Filter.restaurant.translate(context),
                            style: TextStyle(
                              color: _selectedFilter == Filter.restaurant
                                  ? Colors.white
                                  : Colors.black,
                            ),
                          ),
                          selected: _selectedFilter == Filter.restaurant,
                          selectedColor: Colors.blue,
                          onSelected: (value) =>
                              _changeFilter(Filter.restaurant),
                        ),
                      ],
                    ),
                  ),
                  BlocBuilder<SearchBloc, SearchState>(
                    bloc: _searchBloc,
                    builder: (context, state) {
                      if (state is SearchLoadingState) {
                        return const SizedBox(
                          height: 300,
                          child: CustomLoader(),
                        );
                      }
                      if (state is SearchLoadSucces) {
                        return state.response.isNotEmpty
                            ? SearchItems(
                                destinations: state.response,
                              )
                            : Center(
                                child: Text(AppLocalizations.of(context)!
                                    .searchNoItemsMessage),
                              );
                      }

                      return Center(
                          child:
                              Text(AppLocalizations.of(context)!.searchItems));
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _changeFilter(Filter filter) {
    setState(() {
      _selectedFilter = filter;
      if (_searchController.text.length > 1) {
        _searchBloc.add(SearchItemsEvent(
            query: _searchController.text, filter: _selectedFilter));
      }
    });
  }
}
