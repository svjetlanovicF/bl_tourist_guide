part of 'authentication_bloc.dart';

@immutable
sealed class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AuthenticationSignIn extends AuthenticationEvent {
  const AuthenticationSignIn({required this.email, required this.password});

  final String email;
  final String password;

  @override
  List<Object> get props => [email, password];
}

class AuthenticationSignUp extends AuthenticationEvent {
  const AuthenticationSignUp({required this.user});

  final User user;

  @override
  List<Object> get props => [user];
}

class AuthenticationEmailChanged extends AuthenticationEvent {
  const AuthenticationEmailChanged({required this.newValue});

  final String newValue;

  @override
  List<Object> get props => [newValue];
}

class AuthenticationPasswordChanged extends AuthenticationEvent {
  const AuthenticationPasswordChanged({required this.newValue});

  final String newValue;

  @override
  List<Object> get props => [newValue];
}

class AuthenticationFullnameChanged extends AuthenticationEvent {
  const AuthenticationFullnameChanged({required this.newValue});

  final String newValue;

  @override
  List<Object> get props => [newValue];
}

class AuthenticationSignInError extends AuthenticationEvent {}

class AuthenticationNavigateToScreen extends AuthenticationEvent {
  const AuthenticationNavigateToScreen(
      {required this.context, required this.screen});

  final BuildContext context;
  final Widget screen;
}
