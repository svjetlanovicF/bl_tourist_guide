import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../../extensions.dart';
import '../../../../screens/custom_page_route.dart';
import '../../../../service_locator.dart';
import '../../../../services/supabase_service.dart';
import '../../data/user.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(const AuthenticationInitial()) {
    on<AuthenticationSignIn>(_signIn);
    on<AuthenticationEmailChanged>(_emailChanged, transformer: debounce());
    on<AuthenticationPasswordChanged>(_passwordChanged);
    on<AuthenticationNavigateToScreen>(_navigateToScreen);
    on<AuthenticationSignUp>(_signUp);
    on<AuthenticationFullnameChanged>(_fullNameChanged,
        transformer: debounce());
  }

  final _supabaseClient = getIt<SupabaseService>();
  bool _isEmailValid = true;
  bool _isPasswordValid = true;
  bool _isUserTryToSignInOnce = false;
  bool _isFullnameValid = true;

  bool get isEmailValid => _isEmailValid;
  bool get isPasswordValid => _isPasswordValid;
  bool get isFullNameValid => _isFullnameValid;

  Future<void> _signIn(
      AuthenticationSignIn event, Emitter<AuthenticationState> emit) async {
    emit(const AuthenticationInitial());
    _isUserTryToSignInOnce = true;

    if (event.email.isEmailValid && event.password.isPasswordValid) {
      _isEmailValid = true;
      _isPasswordValid = true;

      emit(const AuthenticationInitial(showLoader: true));

      final isLoggedIn =
          await _supabaseClient.signIn(event.email, event.password);

      if (isLoggedIn) {
        emit(AuthenticationLoginSuccesfull());
      } else {
        emit(AuthenticationFailed());
      }
    } else if (!event.email.isEmailValid && !event.password.isPasswordValid) {
      _isEmailValid = false;
      _isPasswordValid = false;
    } else if (!event.email.isEmailValid) {
      _isEmailValid = false;
    } else if (!event.password.isPasswordValid) {
      _isPasswordValid = false;
    } else {
      emit(AuthenticationFailed());
    }
  }

  Future<void> _signUp(
      AuthenticationSignUp event, Emitter<AuthenticationState> emit) async {
    emit(const AuthenticationInitial());
    _isUserTryToSignInOnce = true;

    if (event.user.email.isEmailValid &&
        event.user.password.isPasswordValid &&
        event.user.fullname.trim().isNotEmpty) {
      _isFullnameValid = true;
      _isEmailValid = true;
      _isPasswordValid = true;

      emit(const AuthenticationInitial(showLoader: true));

      final isUserRegister = await _supabaseClient.signUp(event.user);

      if (isUserRegister) {
        emit(AuthenticationRegisterSuccesfull());
      } else {
        emit(AuthenticationFailed());
      }
    } else if (!event.user.email.isEmailValid &&
        !event.user.password.isPasswordValid &&
        event.user.fullname.isEmpty) {
      _isEmailValid = false;
      _isPasswordValid = false;
      _isFullnameValid = false;
    } else if (!event.user.email.isEmailValid) {
      _isEmailValid = false;
    } else if (!event.user.password.isPasswordValid) {
      _isPasswordValid = false;
    } else if (event.user.fullname.trim().isEmpty) {
      _isFullnameValid = false;
    } else {
      emit(AuthenticationFailed());
    }
  }

  void _fullNameChanged(
      AuthenticationFullnameChanged event, Emitter<AuthenticationState> emit) {
    if (!_isUserTryToSignInOnce) {
      return;
    }

    if (event.newValue.trim().isEmpty) {
      _isFullnameValid = false;
      emit(AuthenticationInvalidInput());
    } else {
      _isFullnameValid = true;
      emit(AuthenticationValidInput());
    }
  }

  void _emailChanged(
      AuthenticationEmailChanged event, Emitter<AuthenticationState> emit) {
    if (!_isUserTryToSignInOnce) {
      return;
    }

    if (!event.newValue.isEmailValid) {
      _isEmailValid = false;
      emit(AuthenticationInvalidInput());
    } else {
      _isEmailValid = true;
      emit(AuthenticationValidInput());
    }
  }

  void _passwordChanged(
      AuthenticationPasswordChanged event, Emitter<AuthenticationState> emit) {
    if (!_isUserTryToSignInOnce) {
      return;
    }

    if (!event.newValue.isPasswordValid) {
      emit(AuthenticationInvalidInput());
      _isPasswordValid = false;
    } else {
      _isPasswordValid = true;
      emit(AuthenticationValidInput());
    }
  }

  void _navigateToScreen(
      AuthenticationNavigateToScreen event, Emitter<AuthenticationState> emit) {
    Navigator.of(event.context).push(CustomPageRoute(child: event.screen));
    _isEmailValid = true;
    _isPasswordValid = true;
    _isFullnameValid = true;
    _isUserTryToSignInOnce = false;
    emit(AuthenticationNavigation());
  }
}

EventTransformer<Event> debounce<Event>({
  Duration duration = const Duration(milliseconds: 250),
}) {
  return (events, mapper) => events.debounce(duration).switchMap(mapper);
}
