part of 'authentication_bloc.dart';

@immutable
sealed class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {
  const AuthenticationInitial({this.showLoader = false});
  final bool showLoader;

  @override
  List<Object> get props => [showLoader];
}

class AuthenticationInvalidInput extends AuthenticationState {}

class AuthenticationValidInput extends AuthenticationState {}

class AuthenticationLoginSuccesfull extends AuthenticationState {}

class AuthenticationFailed extends AuthenticationState {}

class AuthenticationNavigation extends AuthenticationState {}

class AuthenticationRegisterSuccesfull extends AuthenticationState {}
