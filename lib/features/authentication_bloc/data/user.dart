class User {
  const User({
    required this.fullname,
    required this.email,
    required this.password,
    this.uid,
  });

  factory User.fromJson({required Map<String, dynamic> json}) => User(
      uid: json['id'],
      fullname: json['fullname'],
      email: json['email'],
      password: json['password']);

  Map<String, dynamic> toJson() => {
        'password': password,
        'fullname': fullname,
      };

  final String? uid;
  final String fullname;
  final String email;
  final String password;
}
