class SupabaseConstants {
  static const String supabaseUrl = 'https://fejgwqreebzqdeeyagko.supabase.co';
  static const String anonKey =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImZlamd3cXJlZWJ6cWRlZXlhZ2tvIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTkyMDQ2MzYsImV4cCI6MjAxNDc4MDYzNn0.BoENYITzIjAfRadTbAFfr3xxQAopLrqn9U2NgPSeg0s';
}

class ImagesConstants {
  static const String blLogo =
      'assets/images/banjaluka-tourist-guide-high-resolution-logo-black.png';
}

class TableNames {
  static const String destinationTable = 'destination';
  static const String touristPlaceTable = 'tourist_place';
  static const String restaurantTable = 'restaurant';
  static const String hotelTable = 'hotel';
  static const String commentTable = 'comment';
  static const String ratingTable = 'rating';
  static const String userTable = 'user';
}

const String favoriteTable = 'favorite';
const String columnId = '_id';
const String columnDestinationId = 'destinationId';
const String columnTitle = 'title';
const String columnFilter = 'filter';
const String columnUserId = 'userId';
