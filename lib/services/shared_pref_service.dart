import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/locale_model.dart';
import '../service_locator.dart';

class SharedPrefService {
  SharedPrefService._internal();

  static SharedPrefService? _instance;
  static SharedPreferences? _preferences;

  static Future<SharedPrefService?> get instance async {
    _instance ??= SharedPrefService._internal();
    _preferences ??= await SharedPreferences.getInstance();

    return _instance;
  }

  Future<void> saveLocale(String language) async {
    await _preferences?.setString('language', language);
  }

  void loadLocale() {
    getIt<LocaleModel>()
        .set(Locale(_preferences?.getString('language') ?? 'en'));
  }
}
