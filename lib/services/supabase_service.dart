import 'dart:developer';

import 'package:supabase_flutter/supabase_flutter.dart';
import '../features/authentication_bloc/data/user.dart' as u;

class SupabaseService {
  final _supabaseClient = Supabase.instance.client;

  SupabaseClient get supabaseClient => _supabaseClient;

  Session? get currentSession => _supabaseClient.auth.currentSession;

  User? get currentUser => _supabaseClient.auth.currentUser;

  Future<bool> signUp(u.User user) async {
    try {
      final authResp = await _supabaseClient.auth.signUp(
          email: user.email, password: user.password, data: user.toJson());

      if (authResp.session != null) {
        return true;
      }

      return false;
    } on AuthException catch (error) {
      log(error.message);
      return false;
    }
  }

  Future<void> signOut() async {
    await _supabaseClient.auth.signOut();
  }

  Future<bool> signIn(String email, String password) async {
    try {
      final authResponse = await _supabaseClient.auth
          .signInWithPassword(email: email, password: password);

      if (authResponse.session != null) {
        return true;
      }

      return false;
    } on AuthException catch (error) {
      log(error.message);
      return false;
    }
  }
}
