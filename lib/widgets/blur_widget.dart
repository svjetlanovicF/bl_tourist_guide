import 'dart:ui';

import 'package:flutter/material.dart';

class BlurWidget extends StatelessWidget {
  const BlurWidget({required this.child, super.key});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 5),
      child: child,
    ));
  }
}
