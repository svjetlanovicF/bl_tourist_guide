import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'items_divider.dart';

class UrlSection extends StatelessWidget {
  const UrlSection({required this.url, required this.title, super.key});

  final String url;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(fontSize: 20),
        ),
        const SizedBox(height: 16),
        GestureDetector(
          onTap: _launchUrl,
          child: Text(
            url,
            style: const TextStyle(
                decoration: TextDecoration.underline, color: Colors.blue),
          ),
        ),
        const ItemsDivider(),
      ],
    );
  }

  Future<void> _launchUrl() async {
    final uri = Uri.parse(url);
    await launchUrl(uri);
  }
}
