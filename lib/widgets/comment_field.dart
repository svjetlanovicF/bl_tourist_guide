import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CommentField extends StatefulWidget {
  const CommentField({
    required this.child,
    required this.controller,
    required this.onSubmit,
    super.key,
  });

  final Widget child;
  final TextEditingController controller;
  final Function() onSubmit;

  @override
  State<CommentField> createState() => _CommentFieldState();
}

class _CommentFieldState extends State<CommentField> {
  final _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _focusNode.unfocus,
      child: Stack(
        children: [
          Positioned.fill(child: widget.child),
          Positioned(
            bottom: 0,
            child: Container(
              padding: const EdgeInsets.only(left: 16),
              decoration: BoxDecoration(
                color: Colors.blue.shade100,
                borderRadius: BorderRadius.circular(12),
              ),
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: TextField(
                      controller: widget.controller,
                      focusNode: _focusNode,
                      maxLines: null,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: AppLocalizations.of(context)!.typeComment,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10).copyWith(right: 0),
                    child: IconButton(
                      onPressed: () {
                        widget.onSubmit();
                        _focusNode.unfocus();
                      },
                      icon: const Icon(
                        Icons.chevron_right,
                        color: Colors.black,
                        size: 30,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
