import 'package:flutter/material.dart';

class ItemsDivider extends StatelessWidget {
  const ItemsDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 16),
        Divider(height: 16),
      ],
    );
  }
}
