import 'package:flutter/material.dart';

import '../enums.dart';

class ListItem extends StatelessWidget {
  const ListItem({required this.title, required this.filter, super.key});

  final String title;
  final Filter filter;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: ListTile(
        leading: const Icon(Icons.restaurant),
        title: Text(title),
        trailing: const Icon(Icons.chevron_right),
      ),
    );
  }
}
