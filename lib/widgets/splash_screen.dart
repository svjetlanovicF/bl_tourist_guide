import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../features/dashboard_bloc/domain/bloc/bloc/dashboard_bloc.dart';
import '../features/dashboard_bloc/ui/screens/dashobard_screen.dart';
import '../service_locator.dart';
import 'custom_loader.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _dashboardBloc = getIt<DashboardBloc>();

  @override
  void initState() {
    super.initState();

    _dashboardBloc.add(DashboardFetchDashboardModels());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey.shade300,
      body: BlocBuilder<DashboardBloc, DashboardState>(
        bloc: _dashboardBloc,
        builder: (context, state) {
          if (state is DashboardLoadSucces) {
            return DashboardScreen(dashboardResponse: state.dashboardResponse);
          }

          return const CustomLoader();
        },
      ),
    );
  }
}
