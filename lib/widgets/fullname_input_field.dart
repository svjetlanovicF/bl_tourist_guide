import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../features/authentication_bloc/domain/bloc/authentication_bloc.dart';
import '../service_locator.dart';
import '../styles/custom_colors.dart';

class FullnameInputField extends StatefulWidget {
  const FullnameInputField({
    required this.controller,
    super.key,
  });

  final TextEditingController controller;

  @override
  State<FullnameInputField> createState() => _FullnameInputFieldState();
}

class _FullnameInputFieldState extends State<FullnameInputField> {
  final _authBloc = getIt<AuthenticationBloc>();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 12).copyWith(bottom: 12),
      width: MediaQuery.of(context).size.width,
      child: BlocSelector<AuthenticationBloc, AuthenticationState, bool>(
        selector: (_) => _authBloc.isFullNameValid,
        bloc: _authBloc,
        builder: (_, state) {
          return TextField(
            controller: widget.controller,
            textInputAction: TextInputAction.next,
            onChanged: (value) =>
                _authBloc.add(AuthenticationFullnameChanged(newValue: value)),
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              error: !state
                  ? Text(
                      AppLocalizations.of(context)!.invalidFullnameMessage,
                      style: const TextStyle(color: Colors.red, fontSize: 14),
                      maxLines: 2,
                    )
                  : null,
              hintText: AppLocalizations.of(context)!.fullname,
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
              focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                borderSide: BorderSide(
                  color: CustomColors.focusedBorderColor,
                  width: 2,
                ),
              ),
              errorBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 2,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
