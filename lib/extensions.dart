extension StringExtensions on String {
  String dividePerWord() {
    return replaceAll(RegExp(' '), '\n');
  }

  bool get isEmailValid => RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(this);

  bool get isPasswordValid => length > 5;
}
