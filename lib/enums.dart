import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum Filter {
  all,
  touristPlace,
  restaurant,
  hotel;

  static Filter fromString(String filterName) => switch (filterName) {
        'Tourist Place' => touristPlace,
        'Restaurant' => restaurant,
        'Hotel' => hotel,
        _ => all,
      };

  String translate(BuildContext context) => switch (this) {
        Filter.all => AppLocalizations.of(context)!.all,
        Filter.touristPlace => AppLocalizations.of(context)!.touristPlace,
        Filter.restaurant => AppLocalizations.of(context)!.restaurant,
        Filter.hotel => AppLocalizations.of(context)!.hotel
      };
}
