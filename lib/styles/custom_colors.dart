import 'package:flutter/material.dart';

class CustomColors {
  static const primaryLightColor = Color(0xFFFFFFFF);
  static const primaryBackgroundLightColor = Color(0xFFE6E3E3);
  static const focusedBorderColor = Color(0XFF2006BE);
}
