import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../features/dashboard_bloc/data/models/hotel.dart';
import '../features/dashboard_bloc/ui/widgets/hotels_list.dart';

class HotelScreen extends StatelessWidget {
  const HotelScreen({required this.hotels, super.key});

  final List<Hotel> hotels;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey.shade300,
        appBar: AppBar(
          backgroundColor: Colors.grey.shade300,
          title: Text(AppLocalizations.of(context)!.hotels),
          centerTitle: true,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: HotelsList(hotels: hotels),
          ),
        ),
      ),
    );
  }
}
