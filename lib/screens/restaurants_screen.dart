import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../features/dashboard_bloc/data/models/restaurant.dart';
import '../features/dashboard_bloc/ui/widgets/restaurants_items_list.dart';

class RestaurantsScreen extends StatelessWidget {
  const RestaurantsScreen({required this.restaurants, super.key});

  final List<Restaurant> restaurants;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey.shade300,
        appBar: AppBar(
          backgroundColor: Colors.grey.shade300,
          title: Text(AppLocalizations.of(context)!.restaurants),
          centerTitle: true,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: RestaurantsItemsList(restaurants: restaurants),
          ),
        ),
      ),
    );
  }
}
