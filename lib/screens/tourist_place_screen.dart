import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../app_state.dart';
import '../features/comment_bloc/data/models/comment.dart';
import '../features/comment_bloc/domain/bloc/comment_bloc.dart';
import '../features/comment_bloc/ui/widgets/comment_section.dart';
import '../features/dashboard_bloc/data/models/tourist_place.dart';
import '../features/favorite_bloc/ui/widgets/favorite_icon.dart';
import '../service_locator.dart';
import '../services/supabase_service.dart';
import '../widgets/blur_widget.dart';
import '../widgets/comment_field.dart';
import '../widgets/custom_loader.dart';
import '../widgets/items_divider.dart';
import '../widgets/location_section.dart';

class TouristPlaceScreen extends StatefulWidget {
  const TouristPlaceScreen(
      {required this.touristPlace, this.isFromFavoriteList = false, super.key});

  final TouristPlace touristPlace;
  final bool isFromFavoriteList;

  @override
  State<TouristPlaceScreen> createState() => _TouristPlaceScreenState();
}

class _TouristPlaceScreenState extends State<TouristPlaceScreen> {
  final _commentBloc = getIt<CommentBloc>();
  final _commentController = TextEditingController();

  @override
  void didChangeDependencies() {
    _commentBloc.add(FetchComments(destinationId: widget.touristPlace.id));
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _commentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CommentBloc, CommentState>(
        bloc: _commentBloc,
        builder: (context, state) {
          if (state is CommentsFetchLoadSucces) {
            return NestedScrollView(
              physics: const PageScrollPhysics(),
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [
                  SliverAppBar(
                    centerTitle: true,
                    leading: BlurWidget(
                      child: IconButton(
                          onPressed: () => Navigator.pop(context),
                          icon: const Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          )),
                    ),
                    title: Text(
                      widget.touristPlace.title,
                      style: const TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    actions: [
                      FavoriteIcon(
                        isFromFavoriteList: widget.isFromFavoriteList,
                        id: widget.touristPlace.id,
                        title: widget.touristPlace.title,
                        type: widget.touristPlace.filter,
                      ),
                    ],
                    expandedHeight: 300,
                    floating: true,
                    pinned: true,
                    snap: true,
                    flexibleSpace:
                        LayoutBuilder(builder: (context, constraints) {
                      return Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: CachedNetworkImage(
                              imageUrl: widget.touristPlace.image ?? '',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      );
                    }),
                  ),
                ];
              },
              body: CommentField(
                controller: _commentController,
                onSubmit: _onSubmit,
                child: SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  physics: const NeverScrollableScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 16),
                      Text(
                        AppLocalizations.of(context)!.overview,
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(height: 16),
                      Text(widget.touristPlace.description),
                      const ItemsDivider(),
                      if (widget.touristPlace.location != null)
                        UrlSection(
                          url: widget.touristPlace.location!,
                          title: AppLocalizations.of(context)!.location,
                        ),
                      if (state.comments.isNotEmpty)
                        CommentSection(
                          comments: state.comments,
                        ),
                    ],
                  ),
                ),
              ),
            );
          }

          return const CustomLoader();
        },
      ),
    );
  }

  void _onSubmit() {
    final comment = Comment(
      comment: _commentController.text,
      authorId: getIt<SupabaseService>().currentUser?.id ?? '',
      createdAt: DateTime.now(),
      destinationId: widget.touristPlace.id,
      authorName: getIt<AppState>().user?.fullname,
    );

    if (_commentController.text.isNotEmpty) {
      setState(() {
        _commentBloc.add(AddComment(comment: comment));
      });
    }

    _commentController.clear();
  }
}
