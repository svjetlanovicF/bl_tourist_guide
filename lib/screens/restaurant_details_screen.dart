import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../app_state.dart';
import '../features/comment_bloc/data/models/comment.dart';
import '../features/comment_bloc/domain/bloc/comment_bloc.dart';
import '../features/comment_bloc/ui/widgets/comment_section.dart';
import '../features/dashboard_bloc/data/models/restaurant.dart';
import '../features/favorite_bloc/ui/widgets/favorite_icon.dart';
import '../service_locator.dart';
import '../services/supabase_service.dart';
import '../widgets/comment_field.dart';
import '../widgets/custom_loader.dart';
import '../widgets/location_section.dart';
import '../widgets/restaurant_container_section.dart';

class RestaurantDetailsScreen extends StatefulWidget {
  const RestaurantDetailsScreen(
      {required this.restaurant, this.isFromFavoriteList = false, super.key});

  final Restaurant restaurant;
  final bool isFromFavoriteList;

  @override
  State<RestaurantDetailsScreen> createState() =>
      _RestaurantDetailsScreenState();
}

class _RestaurantDetailsScreenState extends State<RestaurantDetailsScreen> {
  final _controller = TextEditingController();
  final _commentBloc = getIt<CommentBloc>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _commentBloc.add(FetchComments(destinationId: widget.restaurant.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.grey.shade300,
        centerTitle: true,
        title: Text(widget.restaurant.title),
        actions: [
          FavoriteIcon(
              isFromFavoriteList: widget.isFromFavoriteList,
              id: widget.restaurant.id,
              title: widget.restaurant.title,
              type: widget.restaurant.filter)
        ],
      ),
      body: SafeArea(
        child: BlocBuilder<CommentBloc, CommentState>(
          bloc: _commentBloc,
          buildWhen: (_, current) =>
              current is CommentInitial || current is CommentsFetchLoadSucces,
          builder: (context, state) {
            if (state is CommentsFetchLoadSucces) {
              return CommentField(
                controller: _controller,
                onSubmit: _onSubmit,
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    children: [
                      RestaurantContainerSection(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!.overview,
                              style: const TextStyle(fontSize: 20),
                            ),
                            const SizedBox(height: 8),
                            Text(widget.restaurant.description),
                          ],
                        ),
                      ),
                      if (widget.restaurant.webUrl != null ||
                          widget.restaurant.location != null) ...[
                        RestaurantContainerSection(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (widget.restaurant.location != null)
                                UrlSection(
                                  title: AppLocalizations.of(context)!.location,
                                  url: widget.restaurant.location!,
                                ),
                              if (widget.restaurant.webUrl != null)
                                UrlSection(
                                  title: AppLocalizations.of(context)!.website,
                                  url: widget.restaurant.webUrl!,
                                ),
                            ],
                          ),
                        ),
                      ],
                      if (widget.restaurant.priceRange != null) ...[
                        RestaurantContainerSection(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(context)!.priceRange,
                                style: const TextStyle(fontSize: 20),
                              ),
                              const SizedBox(height: 8),
                              Text(widget.restaurant.priceRange!),
                            ],
                          ),
                        ),
                        if (state.comments.isEmpty) const SizedBox(height: 100),
                      ],
                      if (state.comments.isNotEmpty)
                        CommentSection(
                          comments: state.comments,
                        ),
                    ],
                  ),
                ),
              );
            }

            return const CustomLoader();
          },
        ),
      ),
    );
  }

  void _onSubmit() {
    final comment = Comment(
      comment: _controller.text,
      authorId: getIt<SupabaseService>().currentUser?.id ?? '',
      createdAt: DateTime.now(),
      destinationId: widget.restaurant.id,
      authorName: getIt<AppState>().user?.fullname,
    );

    if (_controller.text.isNotEmpty) {
      setState(() {
        _commentBloc.add(AddComment(comment: comment));
      });
    }

    _controller.clear();
  }
}
