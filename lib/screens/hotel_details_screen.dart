import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../app_state.dart';
import '../features/comment_bloc/data/models/comment.dart';
import '../features/comment_bloc/domain/bloc/comment_bloc.dart';
import '../features/comment_bloc/ui/widgets/comment_section.dart';
import '../features/dashboard_bloc/data/models/hotel.dart';
import '../features/favorite_bloc/ui/widgets/favorite_icon.dart';
import '../service_locator.dart';
import '../services/supabase_service.dart';
import '../widgets/comment_field.dart';
import '../widgets/custom_loader.dart';
import '../widgets/items_divider.dart';
import '../widgets/location_section.dart';

class HotelDetailsScreen extends StatefulWidget {
  const HotelDetailsScreen(
      {required this.hotel, this.isFromFavoriteList = false, super.key});

  final Hotel hotel;
  final bool isFromFavoriteList;

  @override
  State<HotelDetailsScreen> createState() => _HotelDetailsScreenState();
}

class _HotelDetailsScreenState extends State<HotelDetailsScreen> {
  final _controller = TextEditingController();
  final _commentBloc = getIt<CommentBloc>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _commentBloc.add(FetchComments(destinationId: widget.hotel.id));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<CommentBloc, CommentState>(
          bloc: _commentBloc,
          builder: (context, state) {
            if (state is CommentsFetchLoadSucces) {
              return CommentField(
                controller: _controller,
                onSubmit: _onSubmit,
                child: SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 12),
                        child: Stack(
                          children: [
                            AspectRatio(
                              aspectRatio: 16 / 9,
                              child: ClipRRect(
                                borderRadius: const BorderRadius.vertical(
                                  top: Radius.circular(20),
                                  bottom: Radius.circular(20),
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: widget.hotel.image ?? '',
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                            Positioned(
                              right: 0,
                              top: 0,
                              child: FavoriteIcon(
                                isFromFavoriteList: widget.isFromFavoriteList,
                                id: widget.hotel.id,
                                title: widget.hotel.title,
                                type: widget.hotel.filter,
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 0,
                              child: GestureDetector(
                                onTap: () => Navigator.of(context).pop(),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.only(left: 8, top: 8),
                                  child: CircleAvatar(
                                    backgroundColor:
                                        Colors.white.withOpacity(0.2),
                                    child: const Icon(
                                      Icons.chevron_left,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 8, bottom: 8),
                                child: Text(
                                  widget.hotel.title,
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      RatingBar.builder(
                          ignoreGestures: true,
                          initialRating: widget.hotel.stars,
                          itemBuilder: (context, index) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                          onRatingUpdate: (_) {}),
                      Text(
                        AppLocalizations.of(context)!.overview,
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(height: 16),
                      Text(widget.hotel.description),
                      const ItemsDivider(),
                      if (widget.hotel.location != null) ...[
                        UrlSection(
                          url: widget.hotel.location!,
                          title: AppLocalizations.of(context)!.location,
                        ),
                        if (state.comments.isEmpty) const SizedBox(height: 200),
                      ],
                      if (state.comments.isNotEmpty) ...[
                        CommentSection(
                          comments: state.comments,
                        ),
                      ]
                    ],
                  ),
                ),
              );
            }

            return const CustomLoader();
          },
        ),
      ),
    );
  }

  void _onSubmit() {
    final comment = Comment(
      comment: _controller.text,
      authorId: getIt<SupabaseService>().currentUser?.id ?? '',
      createdAt: DateTime.now(),
      destinationId: widget.hotel.id,
      authorName: getIt<AppState>().user?.fullname,
    );

    if (_controller.text.isNotEmpty) {
      setState(() {
        _commentBloc.add(AddComment(comment: comment));
      });
    }

    _controller.clear();
  }
}
