import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../constants.dart';
import '../features/authentication_bloc/domain/bloc/authentication_bloc.dart';
import '../service_locator.dart';
import '../styles/custom_colors.dart';
import '../widgets/custom_loader.dart';
import '../widgets/email_input_field.dart';
import '../widgets/password_input_field.dart';
import '../widgets/splash_screen.dart';
import '../widgets/submit_button.dart';
import 'custom_page_route.dart';
import 'register_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _authBloc = getIt<AuthenticationBloc>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: BlocListener<AuthenticationBloc, AuthenticationState>(
          bloc: _authBloc,
          listener: (context, state) {
            if (state is AuthenticationInitial && state.showLoader) {
              setState(() {
                _isLoading = true;
              });
            } else if (state is AuthenticationFailed) {
              Fluttertoast.showToast(
                msg: AppLocalizations.of(context)!.errorLoginMessage,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16,
              );

              setState(() {
                _isLoading = false;
              });
            } else if (state is AuthenticationLoginSuccesfull) {
              Navigator.of(context).pushAndRemoveUntil(
                  CustomPageRoute(child: const SplashScreen()), (_) => false);
            } else {
              setState(() {
                _isLoading = false;
              });
            }
          },
          child: Stack(
            children: [
              SingleChildScrollView(
                physics: const ClampingScrollPhysics(),
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 8),
                        child: Image.asset(
                          ImagesConstants.blLogo,
                        ),
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20)
                            .copyWith(bottom: 0),
                        child: Column(
                          children: [
                            EmailInputField(
                              controller: _emailController,
                            ),
                            PasswordInputField(
                              controller: _passwordController,
                            ),
                            SubmitButton(
                              onSubmit: !_isLoading ? _onSubmit : null,
                              title: AppLocalizations.of(context)!
                                  .signIn
                                  .toUpperCase(),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(AppLocalizations.of(context)!.withoutAccount),
                        TextButton(
                          onPressed: () {
                            _authBloc.add(AuthenticationNavigateToScreen(
                                context: context,
                                screen: const RegisterScreen()));
                          },
                          child: Text(
                            AppLocalizations.of(context)!.signUp,
                            style: const TextStyle(
                                color: CustomColors.focusedBorderColor),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              if (_isLoading) const CustomLoader()
            ],
          ),
        ),
      ),
    );
  }

  void _onSubmit() {
    _authBloc.add(AuthenticationSignIn(
        email: _emailController.text, password: _passwordController.text));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
