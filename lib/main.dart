import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'constants.dart';
import 'features/favorite_bloc/data/providers/favorite_provider.dart';
import 'firebase_options.dart';
import 'models/locale_model.dart';
import 'my_bloc_observer.dart';
import 'screens/login_screen.dart';
import 'service_locator.dart';
import 'services/shared_pref_service.dart';
import 'services/supabase_service.dart';
import 'widgets/splash_screen.dart';

Future<void> main() async {
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await Supabase.initialize(
    url: SupabaseConstants.supabaseUrl,
    anonKey: SupabaseConstants.anonKey,
  );

  setupGetIt();

  await getIt<FavoriteProvider>().openConnection();

  Bloc.observer = MyBlocObserver();

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _supabaseService = getIt<SupabaseService>();

  @override
  void initState() {
    super.initState();
    var messageId = '';

    SharedPrefService.instance.then((value) {
      value?.loadLocale();
      FlutterNativeSplash.remove();
    });

    _supabaseService.supabaseClient.auth.onAuthStateChange
        .listen((event) async {
      if (event.event == AuthChangeEvent.signedIn) {
        await FirebaseMessaging.instance.requestPermission();

        await FirebaseMessaging.instance.getAPNSToken();
        final fcmToken = await FirebaseMessaging.instance.getToken();

        if (fcmToken != null) {
          await setFcmToken(fcmToken);
        }

        FirebaseMessaging.onMessage.listen((payload) {
          final notification = payload.notification;

          if (notification != null &&
              messageId != payload.messageId &&
              _supabaseService.currentSession != null) {
            Fluttertoast.showToast(
              msg: '${notification.title}\n${notification.body}',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: Colors.blue,
              textColor: Colors.white,
              fontSize: 16,
            );

            messageId = payload.messageId!;
          }
        });
      }
    });

    FirebaseMessaging.instance.onTokenRefresh.listen((fcmToken) async {
      await setFcmToken(fcmToken);
    });
  }

  Future<void> setFcmToken(String fcmToken) async {
    final userId = _supabaseService.supabaseClient.auth.currentUser?.id;
    if (userId != null) {
      await _supabaseService.supabaseClient
          .from('profiles')
          .upsert({'user_id': userId, 'fcm_token': fcmToken});
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => getIt<LocaleModel>(),
      child: Consumer<LocaleModel>(builder: (context, localeModel, child) {
        return MaterialApp(
          title: 'BL Tourist Guide',
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          debugShowCheckedModeBanner: false,
          locale: localeModel.locale,
          theme: ThemeData(
            splashColor: Colors.transparent,
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: getIt<SupabaseService>().currentSession != null
              ? const SplashScreen()
              : const LoginScreen(),
        );
      }),
    );
  }
}
