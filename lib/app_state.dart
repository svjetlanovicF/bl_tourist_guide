import 'features/authentication_bloc/data/user.dart';

import 'features/dashboard_bloc/data/models/hotel.dart';
import 'features/dashboard_bloc/data/models/restaurant.dart';
import 'features/dashboard_bloc/data/models/tourist_place.dart';

class AppState {
  List<Restaurant> _restaurants = [];
  List<TouristPlace> _touristPlaces = [];
  List<Hotel> _hotels = [];
  User? _user;

  List<Restaurant> get restaurants => _restaurants;
  List<TouristPlace> get touristPlaces => _touristPlaces;
  List<Hotel> get hotels => _hotels;
  User? get user => _user;

  void setAppStateProperties({
    required List<Restaurant>? r,
    required List<TouristPlace>? tr,
    required List<Hotel>? h,
    required User? user,
  }) {
    _restaurants = r ?? _restaurants;
    _touristPlaces = tr ?? _touristPlaces;
    _hotels = h ?? _hotels;
    _user = user;
  }
}
